import logo from "./logo.svg";
import "./App.css";
import { observer } from "mobx-react";
import useStores from "./useStores";
import { Todo } from "./store/tasks";
import TodoCard from "./todo";


// TODO - feel free to modify "App" content and develope the solution
const App = observer(() => {
  const { TasksStore } = useStores();

  const handleToggleSubTodos = (index) => {
    TasksStore.tasks[index].showSubTodos = !TasksStore.tasks[index].showSubTodos;
  };

  const renderSubTodos = (subTodos,parentIndex) => {

    if (subTodos.length === 0) {
      return null;
    }
  
    return (
      <div className="todo-parent">
        <button className="btn" onClick={() => TasksStore.createNewTodo(parentIndex)}>Create New Todo</button>

        {subTodos.map((subTodo, subIndex) => (
          <div key={subTodo.id}>
          <TodoCard
            showSub={subTodo.showSubTodos}
            title={subTodo.id.slice(0,4)}
            down={() => TasksStore.moveTodoDown(subIndex)}
            up={() => TasksStore.moveTodoUp(subIndex)}
            remove={() => TasksStore.deleteTodo(subTodo.id)}
            hassibling={subTodos.length > 1}
            add={() => TasksStore.createSubTodo(subTodo.id, `New Sub Todo ${subTodo.id}`)}
          />
            {renderSubTodos(subTodo.subTodos,subTodo.id)}
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="App-header">
     <h1>{TasksStore.label}</h1>
     

     <div className="todo-parent">
     <button className="btn" onClick={() => TasksStore.createNewTodo()}>Create New Todo</button>

           {TasksStore.tasks.map((todo, index) => (
            <div key={todo.id}>
              <TodoCard
              showSub={todo.showSubTodos && todo.subTodos.length}
              title={`${todo.id.slice(0,4)}`}
              hasChild={todo.subTodos.length}
              hassibling={TasksStore.tasks.length > 1}
              down={() => TasksStore.moveTodoDown(index)}
              up={() => TasksStore.moveTodoUp(index)}
              remove={() => TasksStore.deleteTodo(todo.id,'root')}
              add={() => TasksStore.createSubTodo(todo.id, `New Sub Todo ${index}`)}
              handleToggleSubTodos={() => handleToggleSubTodos(index)}
              />
              {
                todo.showSubTodos > 0 && renderSubTodos(todo.subTodos,todo.id)
              }
            </div>
))}
</div>
    </div>
  );
});

export default App;
