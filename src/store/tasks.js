import { makeObservable, observable, action } from "mobx";
import { v4 as uuidv4 } from "uuid"; // Import UUID library

export class Todo {
  id;
  title;
  showSubTodos = true;
  subTodos = [];

  constructor(title) {
    this.title = title;
    this.id = uuidv4()
    makeObservable(this, {
      id: observable,
      title: observable,
      showSubTodos:observable,
      subTodos: observable,
    });
  }
}

class TasksStore {
  label = "دیجی اکسپرس‌";
  tasks = [];

  constructor() {
    makeObservable(this, {
      label: observable,
      tasks: observable,
      createSubTodo: action,
      createNewTodo:action,
      deleteTodo: action,
      moveTodoDown: action,
      moveTodoUp: action,
    });

    const initialTodo = new Todo("My 0 Todo",);
    this.tasks.push(initialTodo);
  }

  findItemById(array, targetId){
    for (const item of array) {
      if (item.id === targetId) {
        return item;
      }
      if (item.subTodos && item.subTodos.length > 0) {
        const result = this.findItemById(item.subTodos, targetId);
        if (result) {
          return result;
        }
      }
    }
  
    return null;
  };

  removeItemById(array, targetId,root){
    if(root){
      const one =  array.filter((item) => item?.id !== targetId)
      this.tasks = one
    }else{
      array.forEach((item) => {
        if (item.subTodos) {
          item.subTodos = item.subTodos.filter((childItem) => childItem.id !== targetId);
          this.removeItemById(item.subTodos, targetId);
        }
      });

    }
  };

  createSubTodo(parentIndex, subTodoTitle) {
    const newSubTodo = new Todo(subTodoTitle);
    const current = this.findItemById(this.tasks,parentIndex);    
    if(current) current.subTodos.push(newSubTodo);
  }

  createNewTodo(parent){
    const newTodoTitle = `My ${TasksStore.tasks?.length} Todo`; 
    if(parent){
      this.createSubTodo(parent,'sub')
    }else{
      this.tasks.push(new Todo(newTodoTitle));
    }
  }

  deleteTodo(todoIndex,root) {
    this.removeItemById(this.tasks,todoIndex,root);
  }


  swapElementsById(array, id1){
    let index1 = null;
  
    const findIndex = (arr) => {
      for (const item of arr) {
        if (item.id === id1) {
          index1 = arr.indexOf(item);
          break;
        }
  
        if (item.subTodos && item.subTodos.length > 0) {
          findIndex(item.subTodos);
        }
      }
    };
  
    findIndex(array);
  
    if (index1 !== null && index1 < array.length - 1) {
      [array[index1], array[index1 + 1]] = [array[index1 + 1], array[index1]];
    }
  };

  
  moveTodoDown(todoIndex) {
    if (todoIndex < this.tasks.length - 1) {
      const temp = this.tasks[todoIndex];
      this.tasks[todoIndex] = this.tasks[todoIndex + 1];
      this.tasks[todoIndex + 1] = temp;
    }
  }

  moveTodoUp(todoIndex) {
    if (todoIndex > 0) {
      const temp = this.tasks[todoIndex];
      this.tasks[todoIndex] = this.tasks[todoIndex - 1];
      this.tasks[todoIndex - 1] = temp;
    }
  }
}

export default new TasksStore();