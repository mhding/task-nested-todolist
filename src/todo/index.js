import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnglesUp,faAnglesDown, faTrash,faPlus, faArrowRight, faArrowDown } from '@fortawesome/free-solid-svg-icons'

import './style.css'

const TodoCard = ({
  showSub,
  title,
  down,
  up,
  remove,
  add,
  handleToggleSubTodos,
  hasChild,
  hassibling
}) => {
  return (
    <div className='todo flex'>
      
        <div className='todo-left flex'>
          <button
          onClick={handleToggleSubTodos}
          className='todo__button todo__button_none'> 
          { hasChild ?  <FontAwesomeIcon icon={ showSub ? faArrowDown : faArrowRight} /> : <span></span> }
          </button>
          <p className='m-0 todo__title'>{title}</p>
        </div>
        <div className='todo-right flex'>
          {
            hassibling && <>
        <button onClick={down} className='todo__button'><FontAwesomeIcon icon={faAnglesDown} /></button>
        <button onClick={up} className='todo__button'><FontAwesomeIcon icon={faAnglesUp} /></button>
            </>
          }
        <button onClick={remove} className='todo__button'><FontAwesomeIcon icon={faTrash} /></button>
        <button onClick={add} className='todo__button'><FontAwesomeIcon icon={faPlus} /></button>
        

        </div>
    </div>
  )
}

export default TodoCard
